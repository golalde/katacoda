Kaixo, eszenatoki honen bidez Kiroldenda datu basea instalatu gabe erabili dezakezu.
**Itxaron mysql instalatu bitartean**, eta amaitzerakoan, joan hurrengo pausora datu basea erabiltzeko.

![Logo MU](https://upload.wikimedia.org/wikipedia/commons/5/5c/Logo_Mondragon_Unibertsitatea.png)