Itxaron datu basea kargatu arte. Karga amaitzerakoan, erabili 
mysql-ren terminala query-ak egiteko.
```
mysql> select artikuloID from artikuloak;
+------------+
| artikuloID |
+------------+
|       1001 |
|       1002 |
|       1003 |
|       1004 |
|       1005 |
|       1006 |
|       1007 |
|       1008 |
|       1009 |
|       1010 |
|       1012 |
|       1013 |
|       1014 |
|       1015 |
|       1016 |
+------------+
15 rows in set (0.00 sec)
```